package com.guestbook.mvc.service;

import com.guestbook.mvc.model.LoginHistory;
import com.guestbook.mvc.model.User;

import java.util.List;

public interface UserService {
	User getById(long id);
	User getByUserName(String username);
	List<LoginHistory> getUserLoginHistory(User user);
	User login(User user);
	User register(User user);
	void delete(User user);
}
