package com.guestbook.mvc.service.impl;

import com.guestbook.mvc.dao.LoginHistoryDao;
import com.guestbook.mvc.dao.UserDao;
import com.guestbook.mvc.model.LoginHistory;
import com.guestbook.mvc.model.User;
import com.guestbook.mvc.service.UserService;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private LoginHistoryDao loginHistoryDao;

	@Autowired
	private ShaPasswordEncoder encoder;

	@Override
	@Transactional
	public User getById(long id) {
		return userDao.getById(id);
	}

	@Override
	@Transactional
	public User getByUserName(String username) {
		return userDao.getByUserName(username);
	}

	@Override
	@Transactional
	public List<LoginHistory> getUserLoginHistory(User user) {
		return loginHistoryDao.getUserLoginHistory(user);
	}

	@Override
	@Transactional
	public User login(User submittedUser) {
		User user = userDao.getByUserName(submittedUser.getUsername().toLowerCase());
		if (user != null && encoder.isPasswordValid(
				user.getPassword(), submittedUser.getPassword(), user.getUsername())) {
			LoginHistory loginHistory = new LoginHistory(LocalDateTime.now(), user);
			loginHistoryDao.save(loginHistory);
			return user;
		}
		return null;
	}

	@Override
	@Transactional
	public User register(User user) {
		//TODO validate password length, username etc.
		String username = user.getUsername().toLowerCase();
		User existingUser = userDao.getByUserName(username);
		String password = user.getPassword();
		if (existingUser == null && password.equals(user.getConfirmPassword()) && password.length() >= 6) {
			user.setUsername(username);
			user.setPassword(encoder.encodePassword(user.getPassword(), username));
			return userDao.save(user);
		}
		return null;
	}

	@Override
	@Transactional
	public void delete(User user) {
		userDao.delete(user);
	}
}
