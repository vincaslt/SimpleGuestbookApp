package com.guestbook.mvc.controller;

import com.guestbook.mvc.model.LoginHistory;
import com.guestbook.mvc.service.UserService;

import org.joda.time.DateTimeComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/user")
public class UserHomeController {

	@Autowired
	UserService userService;

	@RequestMapping(value = "index", method = RequestMethod.GET)
	public String index(Model model, HttpSession session) {
		if (session.getAttribute("username") != null) {
			//TODO this is a very awkward way to sort a list...
			List<LoginHistory> history = userService.getUserLoginHistory
				(userService.getByUserName((String) session.getAttribute("username")));
			Collections.sort(history, new Comparator<LoginHistory>() {
				@Override
				public int compare(LoginHistory o1, LoginHistory o2) {
					return DateTimeComparator.getInstance().compare(
						o2.getTime().toDateTime(), o1.getTime().toDateTime());
				}
			});
			if (history.size() > 1)
				model.addAttribute("last_login", history.get(1).getTime().toString("YYYY-MM-dd HH:mm:ss"));
			else
				model.addAttribute("last_login", "no previous logins");
			return "user/index";
		} else return "redirect:/login";
	}
}
