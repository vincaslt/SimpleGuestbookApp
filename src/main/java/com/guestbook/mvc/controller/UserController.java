package com.guestbook.mvc.controller;

import com.guestbook.mvc.model.User;
import com.guestbook.mvc.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class UserController {

	@Autowired
	UserService userService;

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String addUserGET(Model model) {
		model.addAttribute("user", new User());
		return "register";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String addUserPOST(@ModelAttribute User user, Model model) {
		User u = userService.register(user);
		if (u != null) {
			return "redirect:login";
		}
		model.addAttribute("error", "Registration failed");
		return "register";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginUserGET(Model model, HttpSession session) {
		if (session.getAttribute("username") != null)
			return "redirect:user/index";
		model.addAttribute("user", new User());
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginUserPOST(@ModelAttribute User user, HttpSession session, Model model) {
		User u = userService.login(user);
		if (u != null) {
			session.setAttribute("username", u.getUsername());
			return "redirect:user/index";
		}
		model.addAttribute("error", "Login failed");
		return "login";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutUserGET(HttpSession session) {
		session.removeAttribute("username");
		return "redirect:login";
	}
}
