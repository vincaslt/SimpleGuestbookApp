package com.guestbook.mvc.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "users")
public class User {
	@Id
	@Column(name = "id")
	@GeneratedValue
	private long id;

	@Column(name = "username")
	private String username;

	@Column(name = "password")
	private String password;

	@Transient
	private String confirmPassword;

	@OneToMany(mappedBy = "user")
	private List<LoginHistory> loginHistorySet;

	public User() {
	}

	public User(String username, String password, List<LoginHistory> loginHistorySet) {
		this.username = username;
		this.password = password;
		this.loginHistorySet = loginHistorySet;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<LoginHistory> getLoginHistorySet() {
		return loginHistorySet;
	}

	public void setLoginHistorySet(List<LoginHistory> loginHistorySet) {
		this.loginHistorySet = loginHistorySet;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
}
