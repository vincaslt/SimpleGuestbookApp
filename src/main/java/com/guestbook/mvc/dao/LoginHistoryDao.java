package com.guestbook.mvc.dao;

import com.guestbook.mvc.model.LoginHistory;
import com.guestbook.mvc.model.User;

import java.util.List;

public interface LoginHistoryDao {
	List<LoginHistory> getUserLoginHistory(User user);
	LoginHistory save(LoginHistory loginHistory);
	void delete(LoginHistory loginHistory);
}
