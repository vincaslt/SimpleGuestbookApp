package com.guestbook.mvc.dao.impl;

import com.guestbook.mvc.dao.LoginHistoryDao;
import com.guestbook.mvc.model.LoginHistory;
import com.guestbook.mvc.model.User;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LoginHistoryDaoImpl extends BaseDaoImpl implements LoginHistoryDao {
	@Override
	public List<LoginHistory> getUserLoginHistory(User user) {
		//TODO probably bad to select by username?
		return getSession().createQuery("from LoginHistory h where h.user.username = :username")
			.setString("username", user.getUsername()).list();
	}

	@Override
	public LoginHistory save(LoginHistory loginHistory) {
		return (LoginHistory)getSession().merge(loginHistory);
	}

	@Override
	public void delete(LoginHistory loginHistory) {
		getSession().delete(loginHistory);
	}
}
