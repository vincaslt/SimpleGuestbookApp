package com.guestbook.mvc.dao.impl;

import com.guestbook.mvc.dao.UserDao;
import com.guestbook.mvc.model.User;

import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl extends BaseDaoImpl implements UserDao {

	@Override
	public User getById(long id) {
		return (User)getSession().get(User.class, id);
	}

	@Override
	public User getByUserName(String username) {
		return (User)getSession().createQuery("from User u where u.username = :username")
					.setString("username", username)
					.uniqueResult();
	}

	@Override
	public User save(User user) {
		return (User)getSession().merge(user);
	}

	@Override
	public void delete(User user) {
		getSession().delete(user);
	}
}
