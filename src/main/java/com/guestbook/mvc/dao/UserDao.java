package com.guestbook.mvc.dao;

import com.guestbook.mvc.model.User;

public interface UserDao {
	User getById(long id);
	User getByUserName(String username);
	User save(User user);
	void delete(User user);
}
